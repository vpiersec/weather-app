import {
  WEATHER_DATA_ERROR,
  WEATHER_DATA_FETCHING,
  WEATHER_DATA_FETCH_FINISH,
  WEATHER_DELETE_INFO,
  WEATHER_SAVE_INFO
} from './actionTypes'
import { composeApiUrl } from './helpers'

export const setError = value => ({
  type: WEATHER_DATA_ERROR,
  value
})

export const fetchData = city => async (dispatch, getState) => {
  if (!city) {
    return dispatch(setError('No input provided'))
  }

  dispatch(({
    type: WEATHER_DATA_FETCHING
  }))

  try {
    const res = await fetch(composeApiUrl(city))

    if (!res.ok) {
      return dispatch(setError(`Error fetching data, status: ${res.status}`))
    }
    const data = await res.json()

    dispatch(({
      type: WEATHER_DATA_FETCH_FINISH,
      value: {
        city,
        data
      }
    }))
  } catch (err) {
    dispatch(setError('Could not fetch data'))
  }
}

export const saveInfo = id => ({
  type: WEATHER_SAVE_INFO,
  value: id
})

export const deleteInfo = id => ({
  type: WEATHER_DELETE_INFO,
  value: id
})
