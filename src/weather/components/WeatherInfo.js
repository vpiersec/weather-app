import React from 'react'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'

const keyToText = {
  weather: 'Weather',
  weather_description: 'Description',
  temp: 'Temperature',
  feels_like: 'Feels Like',
  temp_min: 'Min Temperature',
  temp_max: 'Max Temperature',
  pressure: 'Pressure',
  humidity: 'Humidity',
  visibility: 'Visibility',
  wind_speed: 'Wind speed',
  clouds: 'Clouds',
  country: 'Country'
}

const keyToUnit = {
  temp: '°C',
  feels_like: '°C',
  temp_min: '°C',
  temp_max: '°C',
  pressure: 'hPa',
  humidity: '%',
  visibility: 'm',
  wind_speed: 'm/s'
}

const WeatherInfo = ({ city, children, icon, info }) => {
  return (
    <List sx={{
      bgcolor: 'background.paper',
      border: '1px solid',
      borderColor: 'grey.300',
      borderRadius: '4px',
      margin: '8px',
      padding: '16px',
      width: 'calc(100% - 32px)'
    }}
    >
      <ListItem alignItems='center'>
        <ListItemAvatar>
          <Avatar alt={`${city} weather`} src={`http://openweathermap.org/img/w/${icon}.png`} />
        </ListItemAvatar>
        <ListItemText primary={city} />
      </ListItem>

      {Object.keys(info).map(key =>
        <ListItemText
          key={key + info[key]}
          sx={{ display: 'flex', justifyContent: 'space-between' }}
          primary={
            <Typography
              sx={{ display: 'inline', mr: '4px', fontWeight: '600' }}
              component='span'
              variant='body2'
              color='text.primary'
            >
              {`${keyToText[key]}:`}
            </Typography>
          }
          secondary={
            <Typography
              sx={{ display: 'inline' }}
              component='span'
              variant='body2'
              color='text.primary'
            >
              {`${info[key]} ${keyToUnit[key] || ''}`}
            </Typography>
          }
        />)}
      {children}
    </List>
  )
}

export default WeatherInfo
