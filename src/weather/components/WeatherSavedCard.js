import React from 'react'
import { useDispatch } from 'react-redux'

import Box from '@mui/material/Box'
import DeleteIcon from '@mui/icons-material/Delete'
import IconButton from '@mui/material/IconButton'

import { deleteInfo } from '../actions'
import WeatherInfo from './WeatherInfo'

const WeatherSavedCard = props => {
  const dispatch = useDispatch()
  const onClick = () => dispatch(deleteInfo(props.id))

  return (
    <Box sx={{ position: 'relative', width: '100%' }}>
      <WeatherInfo {...props}>
        <IconButton sx={{ position: 'absolute', top: 0, right: 0 }} aria-label='delete' onClick={onClick}>
          <DeleteIcon />
        </IconButton>
      </WeatherInfo>
    </Box>
  )
}

export default WeatherSavedCard
