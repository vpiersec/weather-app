import React, { useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Input from '@mui/material/Input'
import Typography from '@mui/material/Typography'

import { fetchData, setError } from '../actions'

const WeatherPage = () => {
  const inputRef = useRef(null)
  const dispatch = useDispatch()
  const error = useSelector(state => state.weather.error)
  const search = () => dispatch(fetchData(inputRef.current.value.trim().toLowerCase()))
  const searchOnEnter = (e) => {
    if (e.key === 'Enter') {
      search()
    }
  }
  const resetError = () => dispatch(setError(''))

  return (
    <Box sx={{ margin: '16px 0 48px 0', position: 'relative' }}>
      <Input
        sx={{
          marginRight: '16px'
        }}
        autoFocus
        error={Boolean(error)}
        inputRef={inputRef}
        onKeyPress={searchOnEnter}
        onFocus={resetError}
        onClick={resetError}
        placeholder='City'
        type='string'
      />
      <Button
        disableElevation
        onClick={search}
        variant='contained'
      >
        Search
      </Button>
      <Box color='error.light' sx={{ position: 'absolute', bottom: -32 }}>
        {error}
      </Box>
    </Box>
  )
}

export default WeatherPage
