import React from 'react'

import Box from '@mui/material/Box'
import WeatherCard from './WeatherCard'
import WeatherCardList from './WeatherCardList'
import WeatherSearch from './WeatherSearch'
import Typography from '@mui/material/Typography'

const WeatherPage = () =>
  (
    <Box sx={{
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      width: '100%'
    }}
    >
      <Typography component='h1' variant='h4'>Weather app</Typography>
      <WeatherCard />
      <WeatherSearch />
      <WeatherCardList />
    </Box>
  )

export default WeatherPage
