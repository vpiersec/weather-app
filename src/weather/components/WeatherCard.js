import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import CircularProgress from '@mui/material/CircularProgress'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'

import { saveInfo } from '../actions'
import { getIsFetching, getSearchResult } from '../selectors'
import WeatherInfo from './WeatherInfo'

const WeatherCard = () => {
  const dispatch = useDispatch()
  const searchResult = useSelector(getSearchResult)
  const searching = useSelector(getIsFetching)
  const onClick = () => dispatch(saveInfo(searchResult.id))

  return (
    <Box sx={{ width: '40%', height: '460px' }}>
      {searching
        ? <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100%' }}><CircularProgress /></Box>
        : searchResult
          ? <WeatherInfo {...searchResult}>
            <Box sx={{ textAlign: 'center' }}>
              <Button color='primary' onClick={onClick} variant='outlined'>Save</Button>
            </Box>
            </WeatherInfo>
          : null}
    </Box>
  )
}

export default WeatherCard
