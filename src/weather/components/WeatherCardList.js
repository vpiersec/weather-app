import React from 'react'
import { useSelector } from 'react-redux'

import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import { getSaved } from '../selectors'
import WeatherSavedCard from './WeatherSavedCard'

const WeatherCardList = () => {
  const saved = useSelector(getSaved)

  if (!saved.length) {
    return null
  }

  return (
    <List sx={{ display: 'flex', flexWrap: 'wrap', width: '100%' }}>
      {saved.map(info => (
        <ListItem sx={{ flex: '1 1 260px' }} key={info.id}>
          <WeatherSavedCard {...info} />
        </ListItem>
      ))}
    </List>
  )
}

export default React.memo(WeatherCardList)
