import {
  WEATHER_DELETE_INFO,
  WEATHER_DATA_ERROR,
  WEATHER_DATA_FETCHING,
  WEATHER_DATA_FETCH_FINISH,
  WEATHER_SAVE_INFO
} from './actionTypes'

const initialState = {
  error: '',
  fetching: false,
  saved: [],
  searchResult: null
}

const formatSearchResult = data => ({
  id: data.id,
  city: data.name,
  icon: data.weather[0].icon,
  info: {
    weather: data.weather[0].main,
    weather_description: data.weather[0].description,
    temp: data.main.temp,
    feels_like: data.main.feels_like,
    temp_min: data.main.temp_min,
    temp_max: data.main.temp_max,
    pressure: data.main.pressure,
    humidity: data.main.humidity,
    visibility: data.visibility,
    wind_speed: data.wind.speed,
    clouds: data.clouds.all,
    country: data.sys.country
  }
})

export default (state = initialState, action) => {
  const { type, value } = action

  switch (type) {
    case WEATHER_DATA_ERROR: {
      return {
        ...state,
        error: value,
        fetching: false
      }
    }
    case WEATHER_DATA_FETCHING:
      return {
        ...state,
        fetching: true
      }
    case WEATHER_DATA_FETCH_FINISH: {
      const { data } = value

      return {
        ...state,
        fetching: false,
        searchResult: {
          ...formatSearchResult(data)
        }
      }
    }
    case WEATHER_SAVE_INFO: {
      if (state.saved.find(item => item.id === state.searchResult.id)) {
        return state
      }

      const newState = [...state.saved]
      newState.push(state.searchResult)

      return {
        ...state,
        saved: newState
      }
    }
    case WEATHER_DELETE_INFO: {
      const newState = state.saved.filter(item => item.id !== value)

      return {
        ...state,
        saved: newState
      }
    }
    default:
      return state
  }
}
