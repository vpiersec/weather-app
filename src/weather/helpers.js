import { API_URL, API_KEY } from './constants'

export const composeApiUrl = str => {
  const url = new URL(API_URL)

  url.searchParams.append('q', str)
  url.searchParams.append('appid', API_KEY)
  url.searchParams.append('units', 'metric')

  return url
}
