export const getSearchResult = state => state.weather.searchResult

export const getSaved = state => state.weather.saved

export const getIsFetching = state => state.weather.fetching
