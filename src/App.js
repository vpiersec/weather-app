import React from 'react'
import Container from '@mui/material/Container'
import WeatherPage from './weather/components/WeatherPage'

const App = () => (
  <Container maxWidth='md' sx={{ bgcolor: 'grey.100', minHeight: '100%' }}>
    <WeatherPage />
  </Container>
)

export default App
