import { applyMiddleware, createStore, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'

import weatherReducer from './weather/reducer'
import weatherMiddleware from './weather/middleware'

const store = createStore(combineReducers({ weather: weatherReducer }), applyMiddleware(thunkMiddleware, weatherMiddleware))

export default store
